import React from "react";
import { data } from "../../constants";
import "./overall-list.scss";

const icons = [
    <i className="bx bx-receipt"></i>,
    <i className="bx bx-user"></i>,
    <i className="bx bx-cube"></i>,
    <i className="bx bx-dollar"></i>,
];
const OverallList = () => {
    return (
        <div className="overall-list">
            {data.overall.map((item, index) => (
                <li className="overall-list__item" key={`overall-${index}`}>
                    <div className="overall-list__item__icon">
                        {icons[index]}
                    </div>
                    <div className="overall-list__item__info">
                        <div className="title">{item.value}</div>
                        <span>{item.title}</span>
                    </div>
                </li>
            ))}
        </div>
    );
};

export default OverallList;
