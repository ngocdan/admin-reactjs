import React, { useEffect, useState } from "react";
import "./sidebar.scss";
import { images } from "../../constants";
import { Link, useLocation } from "react-router-dom";
import sidebarNav from "../../configs/sidebarNav";

const Sidebar = () => {
    const [activeIndex, setActiveIndex] = useState(0);
    const location = useLocation();

    useEffect(() => {
        const currentPath = window.location.pathname.split("/")[1];
        const activeItem = sidebarNav.findIndex(
            (item) => item.section === currentPath
        );
        console.log();
        setActiveIndex(currentPath.length === 0 ? 0 : activeItem);
    }, [location]);

    const closeSidebar = () => {
        document.querySelector(
            ".main__content"
        ).getElementsByClassName.transform = "scale(1) translateX(0)";
        setTimeout(() => {
            document.body.classList.remove("sidebar-open");
            document.querySelector(".main__content").style = "";
        }, 500);
    };

    return (
        <div className="sidebar">
            <div className="sidebar__logo">
                <img src={images.logo} alt="logo" />
                <div className="sidebar-close" onClick={closeSidebar}>
                    <i className="bx bx-x"></i>
                </div>
            </div>
            <div className="sidebar__menu">
                {sidebarNav.map((item, index) => (
                    <Link
                        to={item.link}
                        key={`nav-${index}`}
                        className={`sidebar__menu__item ${
                            activeIndex === index && "active"
                        }`}
                        onClick={closeSidebar}
                    >
                        <div className="sidebar__menu__item__icon">
                            {item.icon}
                        </div>
                        <div className="sidebar__menu__item__txt">
                            {item.text}
                        </div>
                    </Link>
                ))}
                <div className="sidebar__menu__item">
                    <div className="sidebar__menu__item__icon">
                        <i className="bx bx-log-out"></i>
                    </div>
                    <div className="sidebar__menu__item__txt">logout</div>
                </div>
            </div>
        </div>
    );
};

export default Sidebar;
